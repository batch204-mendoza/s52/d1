import{useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {Redirect} from 'react-router-dom';

export default function Login(){
	const [email,setEmail]= useState("");
	const [password, setPassword]= useState("");
	const [isActive, setIsActive]= useState(false);
	const{user,setUser}=useContext(UserContext)

useEffect(() =>{
	if (email !== '' && password !=='') 
	{
		setIsActive(true)
	}else{
		setIsActive(false)
	}

	}, [email,password,])

function loginUser(e){
	e.preventDefault()

	// line below allos us to save a key/value pair to localsorage
	/*//
	localStorage.setItem('email', email)
	setUser({
		email:email
	});
	setEmail("")
	setPassword("")
	alert('logged in ka na')*/

// 	//User Authentication
// router.post("/login", (req,res)=> {
// 	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
// });

fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
		method: "POST"	,
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			email:email,
			password:password
		})
	}).then(res=>res.json())
	.then(data=>{
		console.log(data)
		// if(data){
		// 	alert("duplicate email exists")
		// }
		// else{fetch(`${process.env.REACT_APP_API_URL}/users/register`,
		// 			{
		// 				method: "POST",
		// 				headers: {
		// 					'Content-Type': 'application/json'
		// 				},
		// 				body: JSON.stringify({
		// 					firstName: firstName,
		// 					lastName: lastName,
		// 					email:email,
		// 					mobileNo:mobileNo,
		// 					password:password1
		// 				}) 
		// 			})
		// 			//make sure na after fetch ung .then
		// 			.then(res=> res.json())
		// 			.then(data=>{
		// 				if (data===true){
		// 					alert("succesfully registered")
		// 				}
		// 				else{
		// 					alert("something went wrong")
		// 				}
		// 			})
		// }
	})


	
}
	


	return(
		
		(user.email !== null) ?

		<Redirect to="/"/>
		

		:
		
		<Form onSubmit={e => loginUser(e)} >
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
				type="email"
				placeholder="Enter email"
				value= {email}
				onChange={e=>setEmail(e.target.value)}
				required
				/> 
				<Form.Text>
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
				type="password"
				placeholder="Enter password"
				value= {password}
				onChange={e=>setPassword(e.target.value)}
				required
				/> 
				<Form.Text className="text-muted">
				
				</Form.Text>
			</Form.Group>

		
		
			{isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
			:
			<Button variant="primary" type="submit" id="submitBtn" disabled>
					Submit
				</Button>	
			}
			


		</Form>

	)
}