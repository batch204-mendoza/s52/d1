import {useContext, useEffect} from 'react';
import {Redirect} from 'react-router-dom';
import UserContext from '../UserContext'

export default function Logout(){
	//get the set
	const {setUser} =useContext(UserContext);
	localStorage.clear();
	useEffect(()=>
	  {
		setUser({
			email:null
		})
	  })
	return(
		//redirects the user to the login page
		<Redirect to="/login"/>
		)
}
