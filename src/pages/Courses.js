import {useEffect, useState} from 'react';
//import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';

export default function Courses(){

	const [courseData, setCourseData] = useState([]);

	// check if mock data was captured
	
	// console.log(courseData);
	// console.log(courseData[0]);
// Props- shorthand for "property" since components are considered as objects in react.js
// Props is a way to pass data from a parent component  to a child component.
// it is synonymous to the function parameter
//this is referred to props drilling. passing from parent to child


// set up below sets up the code below only to run upon mounting once
 // see env

/* // need env .local
env enviroment variable file
this variables exist only in your pc
meaning all variables created here will be isolated to the pc that it was created on
env files are only applied on build time or when its just run
*/
	useEffect(() =>{
		// Syntasx for summongin env
		//console.log(process.env.REACT_APP_API_URL)
	fetch(`${process.env.REACT_APP_API_URL}/courses`)
	.then(res => res.json())
	.then(data => {
		console.log(data)
		setCourseData(data)
	})
	},[])

	const courses =courseData.map(course =>{
		return(
			<CourseCard courseProp={course} key={course._id}/>)
	})
	return(
		<>
		<h1>Courses</h1>
		{courses}

		</>
		)
}