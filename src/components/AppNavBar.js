// long method
// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';
//import {Container,Nav, Navbar, NavDropdown} from 'react-Bootstrap'

// Short method
import {useContext} from 'react'
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import {Link,NavLink} from 'react-router-dom';
import UserContext from '../UserContext';
export default function AppNavBar(){

  // acontext object such ass our userConctext can be opened with reactts userContext hook
  const {user, setUser}= useContext(UserContext);

//	return(
	/*	<Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to= "/">Home</Nav.Link>
            <Nav.Link as={NavLink} to= "/courses" >Courses</Nav.Link>
            <Nav.Link as={NavLink} to= "/login" >Login</Nav.Link>
            <Nav.Link as={NavLink} to= "/register" >Register</Nav.Link>
           {/* <NavDropdown title="Dropdown" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Separated link
              </NavDropdown.Item>
          //  </NavDropdown>*///}
//          </Nav>
  //      </Navbar.Collapse>
    //  </Container>
   // </Navbar>*/

//	)
//}


return(
    <Navbar bg="light" expand="lg">
      <Container>
        <Link className="navbar-brand" to="/">Zuitt</Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
      {/*
        - className is use instead class, to specify a CSS class

        - changes for Bootstrap 5
        from mr -> to me
        from ml -> to ms
      */} 
          <Nav className="ms-auto">
            <Link className="nav-link" to="/">Home</Link>
            <Link className="nav-link" to="/courses" exact>Courses</Link>

            {(user.email !== null) ?
              <Link className="nav-link" to="/logout">Logout</Link>
              :
              <>
                <Link className="nav-link" to="/login">Login</Link>
                <Link className="nav-link" to="/register">Register</Link>
              </>
            }
            
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}
